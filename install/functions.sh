#!/bin/bash

#cellar function
cellar() {
  cd $(brew --cellar)/$1
}

aliasg() {
  alias | grep $*
}

bitbucket() {
    curl -s -H "Accept: application/json" -X GET "https://bitbucket.nike.com/rest/api/1.0/projects/$1/repos?limit=999"
}

curlgetjson() {
   curl -s -H "Accept: application/json" -X GET "$@" |json
}

dockerg() {
  docker images | grep $*
}

exe() {
    chmod +x "$@"
}

exercism_submit() {
    exercism submit src/main/scala/$1
}
alias exs='exercism_submit'
alias ex='exercism'

#flussbaum() {
#    JAVA_OPTS="-Xms20g -Xmx40g" java -jar /Users/iwoltr/dev/rstinv/flussbaum/target/scala-2.12/flussbaum-assembly-0.0.1.jar $@
#}
#alias fb='flussbaum'

git_track_all_branches() {
    for branch in $(git branch -r --format='%(refname:short)'); do
        [[ "${branch#*/}" = HEAD ]] && continue
        git show-ref -q --heads "${branch#*/}" || git branch --track "${branch#*/}" "$branch";
    done
    git fetch --all
    git pull --all
}
alias gtb='git_track_all_branches'

gitbranches() {
    for directory in $(find . -type d -depth 1); do
       if [ -d "$directory/.git" ]; then
           echo "Found git repo: $directory"
           cd "$directory"
           git_track_all_branches
           cd -
       fi
    done
}

git_save_current_branch() {
    if [ -z "$1" ]; then
        echo "Syntax: git_save_current_branch <alias_name>"
    else
        if [[ ! -z "$(git branch|grep '\* ')" ]]; then
           echo "Setting git $1"
           git config --global alias.$1 "checkout $(git symbolic-ref -q HEAD | sed -e 's|^refs/heads/||')"
        else
           echo "No current branch found"
        fi
    fi
}

gitdev() {
    git_save_current_branch dev
}
alias gitd=gitdev
git1() {
    git_save_current_branch one
}
git2() {
    git_save_current_branch two
}
git3() {
    git_save_current_branch three
}
git_reset_ccd() {
    git config --global --unset alias.dev
    git config --global --unset alias.one
    git config --global --unset alias.two
}

jcat() {
    cat "src/main/java/$(echo "$1"|sed 's~\.~\/~g').java"
}



killjava() {
    ps -e | grep -v grep | grep java | awk '{print $1}' | xargs kill -9
}
alias javakill=killjava

mcd() {
   mkdir -p $1
   cd $1
}

newtab() {
    if [ $# -ne 1 ]; then
        PATHDIR=`pwd`
    else
        PATHDIR=$1
    fi

/usr/bin/osascript <<-EOF
tell application "iTerm2"
  tell current window
    create tab with default profile command "zsh -c 'cd $PATHDIR; clear; zsh -i'"
  end tell
end tell
EOF
}

remove_idea(){
    rm -rf .idea
    rm -rf *.iml
    rm -rf *.ipr
    rm -rf *.iws
}
alias rmidea='remove_idea >/dev/null 2>&1'


gradleall() {
#Builds all gradle enabled projects in a certain folder
    for gradle_project in $(find . -name gradlew -depth 2 -type f); do
      DR=$(dirname ${gradle_project})
      echo "building ${DR}"
      cd ${DR}
      ./gradlew build
      cd -
    done
}

sbtall() {
#Compile all the scala enabled projects inside a certain folder
    for gradle_project in $(find . -name build.sbt -depth 2 -type f); do
      DR=$(dirname ${gradle_project})
      echo "building ${DR}"
      cd ${DR}
      sbt compile
      cd -
    done
}

scriptPath() {
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ ${SOURCE} != /* ]] && SOURCE="$SCRIPT_DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
}

zz() {
    source ~/.bashrc
    source ~/.zshrc 2>/dev/null
}
