#!/usr/bin/env bash

jwt() {
    if [ -z $1 ]; then
        ${SUPPORT_MAP}/services/jwt_v2.py -h
    else
        ${SUPPORT_MAP}/services/jwt_v2.py $*
    fi
}

jwtc() {
    echo "Getting default test jwt with:"
    cd ${SUPPORT_MAP}/services
    jwt.py -d
    jwt.py|pbcopy
    cd - >/dev/null
    echo "Jwt copied to clipboard"
}
alias jwtt='jwtc'
alias simjwtc=jwtc

jwtp() {
    cd ${SUPPORT_MAP}/services
    AUDIENCE="$(jwt.py -da)"
    echo "Getting production jwt with:"
    echo "Audience   : ${AUDIENCE}"
    echo "Environment: prod"
    jwt.py "${AUDIENCE}" prod|pbcopy
    cd - >/dev/null
    echo "Jwt copied to clipboard"
}
alias alias simjwtp=jwtp

jwta() {
    cd ${SUPPORT_MAP}/services
    jwt.py -a
    cd -
}
alias alias simjwta=jwta

jwtria() {
    cd ${SUPPORT_MAP}/services
    jwt_v2.py -i riaintegration -a riaintegration -e prod|pbcopy
    cd - >/dev/null
    echo "Jwt copied to clipboard"
}
gps() {
  echo "Calling pricinglookup..."
  cd ${SUPPORT_MAP}/services
  pricelookup.py $*
  cd -
}
alias pricelookup=gps
alias pricinglookup=gps
alias simgps=gps

storelocation() {
  cd ${SUPPORT_MAP}/services
  store_location.py $*
  cd -
}
alias simsl=storelocation
alias simstorelocation=storelocation

productcatalog() {
  cd ${SUPPORT_MAP}/services
  product_catalogue.py $*
  cd -
}
alias simpc=productcatalog
alias simproductcatalog=productcatalog

localproductinfo() {
  cd ${SUPPORT_MAP}/services
  rise_localproduct_info_service.py $*
  cd -
}
alias lapi=localproductinfo
alias simlapi=localproductinfo
alias simlocalproductinfo=localproductinfo