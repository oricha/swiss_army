# Onboarding scripted

This script will do installs as explained by the [onboarding document](https://confluence.nike.com/pages/viewpage.action?pageId=205459855) 
on a new Macbook pro

It will also install other tools I think needed by every developer.
