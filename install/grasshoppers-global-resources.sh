#!/bin/bash

export SUPPORT_MAP=$(<${HOME}/.grasshoppers_setup_done)
export SUPPORT_BIN="${SUPPORT_MAP}/bin"

#Scala
SBT_OPTS="-XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256M"

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk$(java -version 2>&1|grep version|awk '{print $3}'|sed 's/"//g').jdk/Contents/Home
#export JAVA_HOME=/Library/Java/Home

# Should be near the end of this file
export PATH=.:${SUPPORT_BIN}:/usr/local/bin:$PATH

# Add Python scripts
export PATH=$PATH:~/Library/Python/3.7/bin:/usr/local/Cellar/python/3.7.4_1/Frameworks/Python.framework/Versions/3.7/bin
