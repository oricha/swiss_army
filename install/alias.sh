#!/usr/bin/env bash

alias httpie='http --verify=no'
alias epoch='python3 ${SUPPORT_MAP}/services/epoch.py'
alias gitpullall='find . -type d -depth 1 -exec echo "{}" \; -exec git -C {} pull \;'
